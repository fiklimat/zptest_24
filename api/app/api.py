from fastapi import FastAPI
from fastapi.responses import HTMLResponse, FileResponse
import uvicorn
from pathlib import Path
import random as rnd
import glob


app = FastAPI()
html_file_path = Path(__file__).parent.joinpath('static').joinpath('index.html')# Define html index path

@app.get("/", response_class=HTMLResponse)
def root():        
    with open(html_file_path, 'r') as file:
        content= file.read()
    return HTMLResponse(content=content,status_code=200)

@app.get("/generate/{filename}/{number}")
def generate(filename:str,number:int):
    nums = [rnd.randint(0,10) for i in range(1,number)]
    # with open (f"api/app/data/{filename}.txt","w") as f: # bez dockeru
    with open (f"data/{filename}.txt","w") as f: # v dockeru
        f.write(str(nums))
    return str(nums)

@app.get("/getfiles/")
def all_subjects():
    # return [path.split('\\')[1] for path in glob.glob("api/app/data/*")] # bez dockeru
    return [path.split('/')[1] for path in glob.glob("data/*")] # v dockeru

@app.get("/getfile/{filename}")
def subject(filename:str):
    # return FileResponse(path=f"api/app/data/{filename}.txt",media_type="text/plain",filename=f"{filename}.txt") # bez dockeru
    return FileResponse(path=f"data/{filename}.txt",media_type="text/plain",filename=f"{filename}.txt") # v dockeru

if __name__ == "__main__":
    # uvicorn.run(app, host="localhost", port=8000) # bez dockeru
    uvicorn.run(app, host="0.0.0.0", port=8000) # v dockeru
